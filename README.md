# FEM for 2D Problems with [Gmsh](https://gmsh.info/) [\[GitLab\]](https://gitlab.onelab.info/gmsh/gmsh)  

This project demonstrates application of the Finite Element Method (FEM), using **_Linear Elements_**, to the following problems: 
* The Ez & H Field distribution for the TMz mode (Dirichlet B.C.) of 2D rectangular and circular cavities
* The 2D distribution of electrostatic potential (Dirichlet B.C.) for shielded stripline, coaxial cable and microstrip geometries
* A simple 1D PDE  
  _This problem is presented in Example 3.2.1 (and the analytical solution is given in Eq. 2.5.16) of Ref [2]._  

## References

_Finite Element Method_  

[1]. 2006. Polycarpou. Introduction to the FEM in Electromagnetics  
[2]. 2006. Reddy. An Introduction to the FEM. 3rd Edition  
[3]. 2002. Jin. The FEM in Electromagnetics. 2nd Edition  

_Gmsh_

[[4].](https://gmsh.info/doc/texinfo/gmsh.html) _Gmsh Reference Manual_  
* [5.4. Post-Processing Scripting Commands](https://gmsh.info/doc/texinfo/gmsh.html#Post_002dprocessing-scripting-commands)  
* [9. Gmsh plugins](https://gmsh.info/doc/texinfo/gmsh.html#Gmsh-plugins)  
* [10.1. MSH File Format](https://gmsh.info/doc/texinfo/gmsh.html#MSH-file-format)  

_Useful_  

[[5].](https://www.geeksforgeeks.org/how-to-generate-mesh-in-python-with-gmsh-module/) _How to Generate mesh in Python with Gmsh module?_  

## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.90  T=0.02 s (704.6 files/s, 57204.1 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                           6             79            104            737
Markdown                         1             32              0            130
Bourne Shell                     7             56              0             93
MATLAB                           1              4              6             38
PowerShell                       1              7              1             12
-------------------------------------------------------------------------------
SUM:                            16            178            111           1010
-------------------------------------------------------------------------------
</pre>

## How to Run

* This project uses [`gmsh`](https://anaconda.org/conda-forge/gmsh) and [`python-gmsh`](https://anaconda.org/conda-forge/python-gmsh) version 4.11 for _mesh generation_ and _post-processing_. The YAML file for creating the conda environment used to run this project is included in run/conda.

* To run simulations:  
  Using simulation `v11` as an example:
  1. `cd` to the main project directory
  2. `./run/sh/run_v11.sh`

## Experiment v11: The TMz mode of a 2D rectangular cavity

|     |     |     |     |     |
|:---:|:---:|:---:|:---:|:---:|
|**Mode_1_Ez**|**Mode_2_Ez**|**Mode_3_Ez**|**Mode_4_Ez**|**Mode_5_Ez**|
|![][v11_1_Ez]|![][v11_2_Ez]|![][v11_3_Ez]|![][v11_4_Ez]|![][v11_5_Ez]|
|**Mode_1_H** |**Mode_2_H** |**Mode_3_H** |**Mode_4_H** |**Mode_5_H** |
|![][v11_1_H] |![][v11_2_H] |![][v11_3_H] |![][v11_4_H] |![][v11_5_H] |
|**Mode_6_Ez**|**Mode_7_Ez**|**Mode_8_Ez**|**Mode_9_Ez**|**Mode_10_Ez**|
|![][v11_6_Ez]|![][v11_7_Ez]|![][v11_8_Ez]|![][v11_9_Ez]|![][v11_10_Ez]|
|**Mode_6_H** |**Mode_7_H** |**Mode_8_H** |**Mode_9_H** |**Mode_10_H** |
|![][v11_6_H] |![][v11_7_H] |![][v11_8_H] |![][v11_9_H] |![][v11_10_H] |

[v11_1_Ez]:experiments/v11/data_1_E.png
[v11_2_Ez]:experiments/v11/data_2_E.png
[v11_3_Ez]:experiments/v11/data_3_E.png
[v11_4_Ez]:experiments/v11/data_4_E.png
[v11_5_Ez]:experiments/v11/data_5_E.png
[v11_6_Ez]:experiments/v11/data_6_E.png
[v11_7_Ez]:experiments/v11/data_7_E.png
[v11_8_Ez]:experiments/v11/data_8_E.png
[v11_9_Ez]:experiments/v11/data_9_E.png
[v11_10_Ez]:experiments/v11/data_10_E.png
[v11_1_H]:experiments/v11/data_1_H.png
[v11_2_H]:experiments/v11/data_2_H.png
[v11_3_H]:experiments/v11/data_3_H.png
[v11_4_H]:experiments/v11/data_4_H.png
[v11_5_H]:experiments/v11/data_5_H.png
[v11_6_H]:experiments/v11/data_6_H.png
[v11_7_H]:experiments/v11/data_7_H.png
[v11_8_H]:experiments/v11/data_8_H.png
[v11_9_H]:experiments/v11/data_9_H.png
[v11_10_H]:experiments/v11/data_10_H.png

## Experiment v12: The TMz mode of a 2D circular cavity

|     |     |     |     |     |
|:---:|:---:|:---:|:---:|:---:|
|**Mode_1_Ez**|**Mode_2_Ez**|**Mode_3_Ez**|**Mode_4_Ez**|**Mode_5_Ez**|
|![][v12_1_Ez]|![][v12_2_Ez]|![][v12_3_Ez]|![][v12_4_Ez]|![][v12_5_Ez]|
|**Mode_1_H** |**Mode_2_H** |**Mode_3_H** |**Mode_4_H** |**Mode_5_H** |
|![][v12_1_H] |![][v12_2_H] |![][v12_3_H] |![][v12_4_H] |![][v12_5_H] |
|**Mode_6_Ez**|**Mode_7_Ez**|**Mode_8_Ez**|**Mode_9_Ez**|**Mode_10_Ez**|
|![][v12_6_Ez]|![][v12_7_Ez]|![][v12_8_Ez]|![][v12_9_Ez]|![][v12_10_Ez]|
|**Mode_6_H** |**Mode_7_H** |**Mode_8_H** |**Mode_9_H** |**Mode_10_H** |
|![][v12_6_H] |![][v12_7_H] |![][v12_8_H] |![][v12_9_H] |![][v12_10_H] |

[v12_1_Ez]:experiments/v12/data_1_E.png
[v12_2_Ez]:experiments/v12/data_2_E.png
[v12_3_Ez]:experiments/v12/data_3_E.png
[v12_4_Ez]:experiments/v12/data_4_E.png
[v12_5_Ez]:experiments/v12/data_5_E.png
[v12_6_Ez]:experiments/v12/data_6_E.png
[v12_7_Ez]:experiments/v12/data_7_E.png
[v12_8_Ez]:experiments/v12/data_8_E.png
[v12_9_Ez]:experiments/v12/data_9_E.png
[v12_10_Ez]:experiments/v12/data_10_E.png
[v12_1_H]:experiments/v12/data_1_H.png
[v12_2_H]:experiments/v12/data_2_H.png
[v12_3_H]:experiments/v12/data_3_H.png
[v12_4_H]:experiments/v12/data_4_H.png
[v12_5_H]:experiments/v12/data_5_H.png
[v12_6_H]:experiments/v12/data_6_H.png
[v12_7_H]:experiments/v12/data_7_H.png
[v12_8_H]:experiments/v12/data_8_H.png
[v12_9_H]:experiments/v12/data_9_H.png
[v12_10_H]:experiments/v12/data_10_H.png

## Experiment v21: The Stripline

|     |     |
|:---:|:---:|
|**Electrostatic Potential**|**Electric Field**|
|![][v21_1]                 |![][v21_2]        |

[v21_1]:experiments/v21/data_1_V.png
[v21_2]:experiments/v21/data_1_E.png

## Experiment v22: The Coax

|     |     |
|:---:|:---:|
|**Electrostatic Potential**|**Electric Field**|
|![][v22_1]                 |![][v22_2]        |

[v22_1]:experiments/v22/data_1_V.png
[v22_2]:experiments/v22/data_1_E.png

## Experiment v22: The Microstrip

|     |     |
|:---:|:---:|
|**Electrostatic Potential**|**Electric Field**|
|![][v23_1]                 |![][v23_2]        |

[v23_1]:experiments/v23/data_1_V.png
[v23_2]:experiments/v23/data_1_E.png

## Experiment v24: The 2D Potential Box

|     |     |
|:---:|:---:|
|**Electrostatic Potential**|**Electric Field**|
|![][v24_1]                 |![][v24_2]        |

[v24_1]:experiments/v24/data_1_V.png
[v24_2]:experiments/v24/data_1_E.png

## Experiment v31: 1D PDE
This is just a simple [1D FEM](experiments/v31/data.png) problem.

