#########################################
### FINITE ELEMENT METHOD (FEM)       ###
### DIRICHLET BOUNDARY VALUE PROBLEMS ###
### MAIN FUNCTION DEFINITIONS         ###
### by: OAMEED NOAKOASTEEN            ###
#########################################

import os
import numpy as np

def set_up_the_problem(PATHS,PARAMS): 
 if PARAMS[0][0]==1:                       # 2D RECTANGULAR CAVITY: TMz MODE (Az, OR EQUIVALENTLY Ez, IS THE UNKNOWN QUANTITY OF INTEREST)
  from meshd import generate_mesh_rectangle  
  freq                         =3e6        # FREQUENCY (MHz)
  k0                           =2*np.pi*(freq)*np.sqrt(PARAMS[1][1]*PARAMS[1][0])
  lc                           =2.0e-2     # mesh size (lc) close to the point
  NAME                         ='rectangle'  
  physical_grpup_1d_1_tag      =11         # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-X-AXIS (Y=0) 1D BOUNDARY (EDGE)
  physical_grpup_1d_1_tag_value=0  
  physical_grpup_1d_2_tag      =12         # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-Y-AXIS (X=a) 1D BOUNDARY (EDGE)
  physical_grpup_1d_2_tag_value=0
  physical_grpup_1d_3_tag      =13         # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-X-AXIS (Y=b) 1D BOUNDARY (EDGE)
  physical_grpup_1d_3_tag_value=0 
  physical_grpup_1d_4_tag      =14         # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-Y-AXIS (X=0) 1D BOUNDARY (EDGE)
  physical_grpup_1d_4_tag_value=0  
  physical_grpup_2d_1_tag      =21         # 2D PHYSICAL GROUP REPRESENTS 2D REGION (SURFACE) WITH UNIFORM CONSTITUTIVE PARAMETERS
  physical_grpup_2d_1_tag_value=[1,1]      # [RELATIVE EPSILON, RELATIVE MU]    
  physical_grpup_tags          =[[physical_grpup_1d_1_tag,
                                  physical_grpup_1d_2_tag,
                                  physical_grpup_1d_3_tag,
                                  physical_grpup_1d_4_tag ],
                                 [physical_grpup_2d_1_tag ] ]
  physical_grpup_tag_values    =[[physical_grpup_1d_1_tag_value,
                                  physical_grpup_1d_2_tag_value,
                                  physical_grpup_1d_3_tag_value,
                                  physical_grpup_1d_4_tag_value ],
                                 [physical_grpup_2d_1_tag_value ] ]  
  generate_mesh_rectangle(PATHS,physical_grpup_tags,lc,NAME)  
  PARAMS[1].append(freq  )
  PARAMS[1].append(k0    )
  PARAMS.append([2, NAME, 'H-Field',"VT","Curl",'E','H'])
  PARAMS.append(physical_grpup_tags      )
  PARAMS.append(physical_grpup_tag_values)
  PARAMS.append([])                        # 2D PARTITION TAG
 else:
  if PARAMS[0][0]==2:                      # 2D CIRCULAR CAVITY: TMz MODE (Az, OR EQUIVALENTLY Ez, IS THE UNKNOWN QUANTITY OF INTEREST)
   from meshd import generate_mesh_circle   
   freq                         =3e6       # FREQUENCY (MHz)
   k0                           =2*np.pi*(freq)*np.sqrt(PARAMS[1][1]*PARAMS[1][0])
   lc                           =5.0e-2    # mesh size (lc) close to the point
   NAME                         ='circle'   
   physical_grpup_1d_1_tag      =11        # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE CIRCLE'S PERIMETER 1D BOUNDARY
   physical_grpup_1d_1_tag_value=0     
   physical_grpup_2d_1_tag      =21        # 2D PHYSICAL GROUP REPRESENTS 2D REGION (SURFACE) WITH UNIFORM CONSTITUTIVE PARAMETERS
   physical_grpup_2d_1_tag_value=[1,1]     # [RELATIVE EPSILON, RELATIVE MU]     
   physical_grpup_tags          =[[physical_grpup_1d_1_tag],
                                  [physical_grpup_2d_1_tag] ]
   physical_grpup_tag_values    =[[physical_grpup_1d_1_tag_value],
                                  [physical_grpup_2d_1_tag_value] ]   
   generate_mesh_circle(PATHS,physical_grpup_tags,lc,NAME)   
   PARAMS[1].append(freq  )
   PARAMS[1].append(k0    )
   PARAMS.append([2, NAME, 'H-Field',"VT","Curl",'E','H'])
   PARAMS.append(physical_grpup_tags      )
   PARAMS.append(physical_grpup_tag_values)
   PARAMS.append([])                       # 2D PARTITION TAG
  else:
   if PARAMS[0][0]==6:
    from meshd import generate_mesh_rectangle
    lc                           =1e-2     # mesh size (lc) close to the point
    NAME                         ='rectangle'  
    physical_grpup_1d_1_tag      =11       # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-X-AXIS (Y=0) 1D BOUNDARY (EDGE)
    physical_grpup_1d_1_tag_value=0  
    physical_grpup_1d_2_tag      =12       # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-Y-AXIS (X=a) 1D BOUNDARY (EDGE)
    physical_grpup_1d_2_tag_value=0
    physical_grpup_1d_3_tag      =13       # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-X-AXIS (Y=b) 1D BOUNDARY (EDGE)
    physical_grpup_1d_3_tag_value=0 
    physical_grpup_1d_4_tag      =14       # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-Y-AXIS (X=0) 1D BOUNDARY (EDGE)
    physical_grpup_1d_4_tag_value=1  
    physical_grpup_2d_1_tag      =21       # 2D PHYSICAL GROUP REPRESENTS 2D REGION (SURFACE) WITH UNIFORM CONSTITUTIVE PARAMETERS
    physical_grpup_2d_1_tag_value=[1,1]    # [RELATIVE EPSILON, RELATIVE MU]    
    physical_grpup_tags          =[[physical_grpup_1d_1_tag,
                                    physical_grpup_1d_2_tag,
                                    physical_grpup_1d_3_tag,
                                    physical_grpup_1d_4_tag ],
                                   [physical_grpup_2d_1_tag ] ]
    physical_grpup_tag_values    =[[physical_grpup_1d_1_tag_value,
                                    physical_grpup_1d_2_tag_value,
                                    physical_grpup_1d_3_tag_value,
                                    physical_grpup_1d_4_tag_value ],
                                   [physical_grpup_2d_1_tag_value ] ]  
    generate_mesh_rectangle(PATHS,physical_grpup_tags,lc,NAME)  
    PARAMS.append([3, NAME, 'E-Field',"ST","Gradient",'V','E'])
    PARAMS.append(physical_grpup_tags      )
    PARAMS.append(physical_grpup_tag_values)
    PARAMS.append([])                      # 2D PARTITION TAG
   else:
    if PARAMS[0][0]==3:
     from meshd import generate_mesh_stripline
     lc                           =1e-2    # mesh size (lc) close to the point
     NAME                         ='stripline'
     physical_grpup_1d_1_tag      =11      # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-X-AXIS (Y=0) 1D BOUNDARY (OUTER EDGE)
     physical_grpup_1d_1_tag_value=0  
     physical_grpup_1d_2_tag      =12      # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-Y-AXIS (X=a) 1D BOUNDARY (OUTER EDGE)
     physical_grpup_1d_2_tag_value=0
     physical_grpup_1d_3_tag      =13      # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-X-AXIS (Y=b) 1D BOUNDARY (OUTER EDGE)
     physical_grpup_1d_3_tag_value=0 
     physical_grpup_1d_4_tag      =14      # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-Y-AXIS (X=0) 1D BOUNDARY (OUTER EDGE)
     physical_grpup_1d_4_tag_value=0       
     physical_grpup_1d_5_tag      =15      # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-X-AXIS (Y=0) 1D BOUNDARY (INNER EDGE)
     physical_grpup_1d_5_tag_value=1  
     physical_grpup_1d_6_tag      =16      # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-Y-AXIS (X=a) 1D BOUNDARY (INNER EDGE)
     physical_grpup_1d_6_tag_value=1
     physical_grpup_1d_7_tag      =17      # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-X-AXIS (Y=b) 1D BOUNDARY (INNER EDGE)
     physical_grpup_1d_7_tag_value=1 
     physical_grpup_1d_8_tag      =18      # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE PRALLEL-TO-Y-AXIS (X=0) 1D BOUNDARY (INNER EDGE)
     physical_grpup_1d_8_tag_value=1  
     physical_grpup_2d_1_tag      =21      # 2D PHYSICAL GROUP REPRESENTS 2D REGION (SURFACE) WITH UNIFORM CONSTITUTIVE PARAMETERS
     physical_grpup_2d_1_tag_value=[1,1]   # [RELATIVE EPSILON, RELATIVE MU]    
     physical_grpup_tags          =[[physical_grpup_1d_1_tag,
                                     physical_grpup_1d_2_tag,
                                     physical_grpup_1d_3_tag,
                                     physical_grpup_1d_4_tag,
                                     physical_grpup_1d_5_tag,
                                     physical_grpup_1d_6_tag,
                                     physical_grpup_1d_7_tag,
                                     physical_grpup_1d_8_tag ],
                                    [physical_grpup_2d_1_tag ] ]
     physical_grpup_tag_values    =[[physical_grpup_1d_1_tag_value,
                                     physical_grpup_1d_2_tag_value,
                                     physical_grpup_1d_3_tag_value,
                                     physical_grpup_1d_4_tag_value,
                                     physical_grpup_1d_5_tag_value,
                                     physical_grpup_1d_6_tag_value,
                                     physical_grpup_1d_7_tag_value,
                                     physical_grpup_1d_8_tag_value ],
                                    [physical_grpup_2d_1_tag_value ] ]  
     generate_mesh_stripline(PATHS,physical_grpup_tags,lc,NAME)  
     PARAMS.append([3, NAME, 'E-Field',"ST","Gradient",'V','E'])
     PARAMS.append(physical_grpup_tags      )
     PARAMS.append(physical_grpup_tag_values)
     PARAMS.append([])                     # 2D PARTITION TAG
    else:
     if PARAMS[0][0]==4:
      from meshd import generate_mesh_coax
      lc                           =5.0e-2 # mesh size (lc) close to the point
      NAME                         ='coax'
      physical_grpup_1d_1_tag      =11     # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE OUTTER CIRCLE'S PERIMETER 1D BOUNDARY
      physical_grpup_1d_1_tag_value=0           
      physical_grpup_1d_2_tag      =12     # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE INNER  CIRCLE'S PERIMETER 1D BOUNDARY
      physical_grpup_1d_2_tag_value=1           
      physical_grpup_2d_1_tag      =21     # 2D PHYSICAL GROUP REPRESENTS 2D REGION (SURFACE) WITH UNIFORM CONSTITUTIVE PARAMETERS
      physical_grpup_2d_1_tag_value=[1,1]  # [RELATIVE EPSILON, RELATIVE MU]     
      physical_grpup_tags          =[[physical_grpup_1d_1_tag,
                                      physical_grpup_1d_2_tag ],
                                     [physical_grpup_2d_1_tag ] ]
      physical_grpup_tag_values    =[[physical_grpup_1d_1_tag_value,
                                      physical_grpup_1d_2_tag_value ],
                                     [physical_grpup_2d_1_tag_value ] ]
      generate_mesh_coax(PATHS,physical_grpup_tags,lc,NAME)  
      PARAMS.append([3, NAME, 'E-Field',"ST","Gradient",'V','E'])
      PARAMS.append(physical_grpup_tags      )
      PARAMS.append(physical_grpup_tag_values)
      PARAMS.append([])                    # 2D PARTITION TAG
     else:
      if PARAMS[0][0]==5:
       from meshd import generate_mesh_microstrip
       lc                           =1e-2  # mesh size (lc) close to the point
       NAME                         ='microstrip'
       partition_tag                =41    # 2D PARTITION TAG
       physical_grpup_1d_1_tag      =11    # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE EDGES OF THE OUTTER BOX 
       physical_grpup_1d_1_tag_value=0  
       physical_grpup_1d_2_tag      =12    # 1D PHYSICAL GROUP REPRESENTS DIRICHLET B.C. ALONG THE EDGES OF THE MICROSTRIP 
       physical_grpup_1d_2_tag_value=1  
       physical_grpup_2d_1_tag      =21    # 2D PHYSICAL GROUP REPRESENTS THE CONSTITUTIVE PARAMETERS OF THE AIR-FILLED 2D REGION
       physical_grpup_2d_1_tag_value=[1,1] # [RELATIVE EPSILON, RELATIVE MU]            
       physical_grpup_2d_2_tag      =22    # 2D PHYSICAL GROUP REPRESENTS THE CONSTITUTIVE PARAMETERS OF THE DIELECTRIC-FILLED 2D REGION
       physical_grpup_2d_2_tag_value=[5,1] # [RELATIVE EPSILON, RELATIVE MU]            
       physical_grpup_tags          =[[physical_grpup_1d_1_tag,
                                       physical_grpup_1d_2_tag ],
                                      [physical_grpup_2d_1_tag,
                                       physical_grpup_2d_2_tag ],
                                      [partition_tag           ] ]
       physical_grpup_tag_values    =[[physical_grpup_1d_1_tag_value,
                                       physical_grpup_1d_2_tag_value ],
                                      [physical_grpup_2d_1_tag_value,
                                       physical_grpup_2d_2_tag_value ] ]
       generate_mesh_microstrip(PATHS,physical_grpup_tags,lc,NAME)  
       PARAMS.append([3, NAME, 'E-Field',"ST","Gradient",'V','E'])
       PARAMS.append(physical_grpup_tags      )
       PARAMS.append(physical_grpup_tag_values)
       PARAMS.append([partition_tag])      # 2D PARTITION TAG

def global_matrix_assembler(PATHS,PARAMS,NODES,ELEMENTS):
 from utilsd import export_global_matrix_unknowns
 def get_alpha_and_beta(PARAMS,ELEMENTS,INDEX):
  element_tag_values= PARAMS[4][1][PARAMS[3][1].index(ELEMENTS[INDEX][3])]
  if PARAMS[2][0]==2:
   alpha=1/element_tag_values[1]
   beta =np.power(PARAMS[1][3],2)*element_tag_values[0]
  else:
   if PARAMS[2][0]==3:
    alpha=element_tag_values[0]
    beta =0
  return alpha, beta
 def get_element_model(NODES,ALPHA,BETA):
  x1          =NODES[0][0]
  y1          =NODES[0][1]
  x2          =NODES[1][0]
  y2          =NODES[1][1]
  x3          =NODES[2][0]
  y3          =NODES[2][1]
  jacobian    =np.array([[x2-x1,y2-y1],
                         [x3-x1,y3-y1] ])
  surfaceAREA =0.5*np.linalg.det(jacobian)
  M11         =np.power(y2-y3,2)+np.power(x3-x2,2)
  M12         =(y2-y3)*(y3-y1)+(x3-x2)*(x1-x3)
  M13         =(y2-y3)*(y1-y2)+(x3-x2)*(x2-x1)
  M22         =np.power(y3-y1,2)+np.power(x1-x3,2)
  M23         =(y3-y1)*(y1-y2)+(x1-x3)*(x2-x1)
  M33         =np.power(y1-y2,2)+np.power(x2-x1,2)
  M           =(-alpha/(4*surfaceAREA))*np.array([[M11,M12,M13],
                                                  [M12,M22,M23],
                                                  [M13,M23,M33] ])
  T           =(beta*surfaceAREA/12   )*np.array([[2  ,1  ,1  ],
                                                  [1  ,2  ,1  ],
                                                  [1  ,1  ,2  ] ])
  return M+T
 global_matrix_unknowns=np.zeros((len(NODES),len(NODES)))
 for index in range(len(ELEMENTS)):
  element           = ELEMENTS[index][:3]
  nodes             =[NODES[i][:3] for i in element]
  alpha, beta       =get_alpha_and_beta(PARAMS,ELEMENTS,index)
  K                 =get_element_model (nodes,alpha,beta)
  for  i in range(3):
   for j in range(3):
    global_matrix_unknowns[element[i],element[j]]+=K[i,j]
 export_global_matrix_unknowns(global_matrix_unknowns,PATHS)
 return global_matrix_unknowns

def impose_dirichlet_boundary_conditions(PARAMS,NODES,TAGS,MATRIX):
 def get_tag_value(TAG,PARAMS):
  if not isinstance(TAG, list):
   index    =PARAMS[3][0].index(TAG)
   tag_value=PARAMS[4][0][index]
  else:
   indeces  =[PARAMS[3][0].index(t) for t     in TAG    ]
   tag_value=[PARAMS[4][0][index]   for index in indeces]
   tag_value=np.mean(tag_value)
  return tag_value
 source_vector  =np.zeros(len(NODES))
 index_of_knowns=[]
 for i in range(len(NODES)):
  tag=NODES[i][3]
  if tag in TAGS:
   index_of_knowns.append(i)
   dirichlet_value=  get_tag_value(tag,PARAMS)
   source_vector += -dirichlet_value*MATRIX[:,i] 
 matrix           =np.delete(MATRIX       , index_of_knowns, axis=0)
 matrix           =np.delete(matrix       , index_of_knowns, axis=1)
 source_vector    =np.delete(source_vector, index_of_knowns, axis=0)
 index_of_unknowns=[idx for idx in range(len(NODES)) if not idx in index_of_knowns]
 return matrix, source_vector, [index_of_knowns,index_of_unknowns]

def get_solution_complete(PARAMS,NODES,INDECES,X):
 def get_dirichlet_values(PARAMS,NODES,INDECES):
  tags  =[NODES[i][3] for i in INDECES]
  values=[]
  for i in range(len(INDECES)):
   tag     =tags[i]
   if not isinstance(tag,list):
    index=PARAMS[3][0].index(tag)
    value=PARAMS[4][0][index]
   else:
    index=[PARAMS[3][0].index(t) for t   in tag  ]
    value=[PARAMS[4][0][idx]     for idx in index]
    value=np.mean(value)
   values.append(value)
  return values
 index_of_knowns  =INDECES[0]
 index_of_unknowns=INDECES[1]
 size             =len(NODES)
 length           =X.shape[1]
 solution         =np.zeros((size,length)) 
 dirichlet_values =get_dirichlet_values(PARAMS,NODES,index_of_knowns)
 for  i in range(length):
  for j in range(len(index_of_knowns  )):
   solution[index_of_knowns[j]  ,i]=dirichlet_values[j]
  for j in range(len(index_of_unknowns)):
   solution[index_of_unknowns[j],i]=X[j,i]
 return solution

def post_processing(PATHS,PARAMS,NODES,ELEMENTS,SOLUTION):
 def get_data(NODES,ELEMENTS,SOLUTION,PARAMS):
  def get_data_scalar(node_0,node_1,node_2,element,solution):
   return [node_0[0], node_1[0], node_2[0],
           node_0[1], node_1[1], node_2[1],
           node_0[2], node_1[2], node_2[2],  
           solution[element[0]]           ,
           solution[element[1]]           ,
           solution[element[2]]            ]
  def get_data_vector(node_0,node_1,node_2,element,solution):
   return [node_0[0], node_1[0], node_2[0]           ,
           node_0[1], node_1[1], node_2[1]           ,
           node_0[2], node_1[2], node_2[2]           ,  
           0        , 0        , solution[element[0]],
           0        , 0        , solution[element[1]],
           0        , 0        , solution[element[2]] ]
  data=[]
  for i in range(len(ELEMENTS)):
   element=ELEMENTS[i]         [:3]
   node_0 =NODES   [element[0]][:3]
   node_1 =NODES   [element[1]][:3]
   node_2 =NODES   [element[2]][:3]
   if  PARAMS[0][0] in [1,2    ]:
    data   =data+get_data_vector(node_0,node_1,node_2,element,SOLUTION)
   else:
    if PARAMS[0][0] in [3,4,5,6]:
     data  =data+get_data_scalar(node_0,node_1,node_2,element,SOLUTION)
  return data
 import gmsh 
 for  i in range(SOLUTION.shape[1]):
  data       =get_data       (NODES,ELEMENTS,SOLUTION[:,i], PARAMS)
  gmsh.initialize()
  view1      =gmsh.view.add  (      PARAMS[2][2]                      )
  gmsh.view.addListData      (view1,PARAMS[2][3],len(ELEMENTS),data   )
  gmsh.view.write            (view1, os.path.join(PATHS[0],'data'+'_'+str(i+1)+'_'+PARAMS[2][5]+'.pos'))  
  if PARAMS[0][0] in [3,4,5,6]:
   gmsh.plugin.setNumber     (      "MathEval"  ,"View"       ,view1  )
   gmsh.plugin.setString     (      "MathEval"  ,"Expression0","-1*v0")
   view1     =gmsh.plugin.run(      "MathEval"                        )   
  gmsh.plugin.setNumber      (      PARAMS[2][4],"View"       ,view1  )
  view2      =gmsh.plugin.run(      PARAMS[2][4]                      ) 
  gmsh.view.write            (view2, os.path.join(PATHS[0],'data'+'_'+str(i+1)+'_'+PARAMS[2][6]+'.pos'))
  #gmsh.fltk.run() # gmsh.view.option.setNumber (view1, "Visible", 0)
  gmsh.finalize()


