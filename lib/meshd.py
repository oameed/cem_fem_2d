#########################################
### FINITE ELEMENT METHOD (FEM)       ###
### DIRICHLET BOUNDARY VALUE PROBLEMS ###
### MESH FUNCTION DEFINITIONS         ###
### by: OAMEED NOAKOASTEEN            ###
#########################################

import os
import numpy as np

def generate_mesh_rectangle(PATHS,PHYSTAGS,LC,NAME):
 import gmsh
 filename               =os.path.join(PATHS[0],NAME+'.msh')
 gmsh.initialize()                                           # INITIALIZE GMSH
 gmsh.option.setNumber("General.Terminal"   ,1   )           # MAKE VERBOSE
 gmsh.option.setNumber("Mesh.MshFileVersion",4.11)           # SET MESH OUTPUT VERSION
 #gmsh.option.setNumber("Mesh.SaveAll"       ,1)             # save all mesh elements whether or not physical groups are defined
 gmsh.model.add(NAME)                                        # ADD MODEL
 gmsh.model.geo.addPoint(0,0  ,0,LC,1 )                      # BOTTOM LEFT  CORNER
 gmsh.model.geo.addPoint(1,0  ,0,LC,2 )                      # BOTTOM RIGHT CORNER
 gmsh.model.geo.addPoint(1,0.5,0,LC,3 )                      # TOP    RIGHT CORNER
 gmsh.model.geo.addPoint(0,0.5,0,LC,4 )                      # TOP    LEFT  CORNER
 gmsh.model.geo.addLine(1,2,1)
 gmsh.model.geo.addLine(2,3,2)
 gmsh.model.geo.addLine(3,4,3)
 gmsh.model.geo.addLine(4,1,4)
 gmsh.model.geo.addCurveLoop([1,2,3,4],1)
 gmsh.model.geo.addPlaneSurface([1],1)
 gmsh.model.addPhysicalGroup(1, [1], PHYSTAGS[0][0])
 gmsh.model.addPhysicalGroup(1, [2], PHYSTAGS[0][1])
 gmsh.model.addPhysicalGroup(1, [3], PHYSTAGS[0][2])
 gmsh.model.addPhysicalGroup(1, [4], PHYSTAGS[0][3])
 gmsh.model.addPhysicalGroup(2, [1], PHYSTAGS[1][0])
 gmsh.model.geo.synchronize() 
 gmsh.model.mesh.generate(2)                                 # GENERATE 2D MESH
 #gmsh.fltk.run()                                            # VISUALIZE MESH
 gmsh.write(filename)                                        # SAVE MESH FILE
 gmsh.finalize()                                             # FINALIZE GMSH

def generate_mesh_circle(PATHS,PHYSTAGS,LC,NAME):
 import gmsh
 filename               =os.path.join(PATHS[0],NAME+'.msh')
 gmsh.initialize()                                           # INITIALIZE GMSH
 gmsh.option.setNumber("General.Terminal"   ,1   )           # MAKE VERBOSE
 gmsh.option.setNumber("Mesh.MshFileVersion",4.11)           # SET MESH OUTPUT VERSION
 #gmsh.option.setNumber("Mesh.SaveAll"       ,1)             # save all mesh elements whether or not physical groups are defined
 gmsh.model.add(NAME)                                        # ADD MODEL
 gmsh.model.geo.addPoint( 0, 0, 0, LC, 1)       
 gmsh.model.geo.addPoint(-1, 0, 0, LC, 2)
 gmsh.model.geo.addPoint( 1, 0, 0, LC, 3)
 gmsh.model.geo.addCircleArc(2,1,3,1)
 gmsh.model.geo.addCircleArc(3,1,2,2)
 gmsh.model.geo.addCurveLoop([1,2],1)
 gmsh.model.geo.addPlaneSurface([1], 1)
 gmsh.model.addPhysicalGroup(1, [1,2], PHYSTAGS[0][0]) 
 gmsh.model.addPhysicalGroup(2, [1  ], PHYSTAGS[1][0])
 gmsh.model.geo.synchronize() 
 gmsh.model.mesh.generate(2)                                 # GENERATE 2D MESH
 #gmsh.fltk.run()                                            # VISUALIZE MESH
 gmsh.write(filename)                                        # SAVE MESH FILE
 gmsh.finalize()                                             # FINALIZE GMSH

def generate_mesh_stripline(PATHS,PHYSTAGS,LC,NAME):
 import gmsh
 filename=os.path.join(PATHS[0],NAME+'.msh')
 h       =0.375
 w       =0.250
 t       =0.125  
 gmsh.initialize()                                           # INITIALIZE GMSH
 gmsh.option.setNumber("General.Terminal"   ,1   )           # MAKE VERBOSE
 gmsh.option.setNumber("Mesh.MshFileVersion",4.11)           # SET MESH OUTPUT VERSION
 #gmsh.option.setNumber("Mesh.SaveAll"       ,1)             # save all mesh elements whether or not physical groups are defined
 gmsh.model.add(NAME)                                        # ADD MODEL
 gmsh.model.geo.addPoint(0      ,0    , 0, 2*LC, 1)          # BOUNDING BOX: BOTTOM LEFT
 gmsh.model.geo.addPoint(1      ,0    , 0, 2*LC, 2)          # BOUNDING BOX: BOTTOM RIGHT
 gmsh.model.geo.addPoint(1      ,1    , 0, 2*LC, 3)          # BOUNDING BOX: TOP    RIGHT
 gmsh.model.geo.addPoint(0      ,1    , 0, 2*LC, 4)          # BOUNDING BOX: TOP    LEFT
 gmsh.model.geo.addPoint(0.5-w/2,h-t/2, 0,   LC, 5)          # STRIP       : BOTTOM LEFT
 gmsh.model.geo.addPoint(0.5+w/2,h-t/2, 0,   LC, 6)          # STRIP       : BOTTOM RIGHT
 gmsh.model.geo.addPoint(0.5+w/2,h+t/2, 0,   LC, 7)          # STRIP       : TOP    RIGHT
 gmsh.model.geo.addPoint(0.5-w/2,h+t/2, 0,   LC, 8)          # STRIP       : TOP    LEFT
 gmsh.model.geo.addLine (1,2,1)
 gmsh.model.geo.addLine (2,3,2)
 gmsh.model.geo.addLine (3,4,3)
 gmsh.model.geo.addLine (4,1,4)
 gmsh.model.geo.addLine (5,6,5)
 gmsh.model.geo.addLine (6,7,6)
 gmsh.model.geo.addLine (7,8,7)
 gmsh.model.geo.addLine (8,5,8)
 gmsh.model.geo.addCurveLoop([1,2,3,4],1)
 gmsh.model.geo.addCurveLoop([5,6,7,8],2)
 gmsh.model.geo.addPlaneSurface([1,2], 1) 
 gmsh.model.addPhysicalGroup(1, [1], PHYSTAGS[0][0]) 
 gmsh.model.addPhysicalGroup(1, [2], PHYSTAGS[0][1]) 
 gmsh.model.addPhysicalGroup(1, [3], PHYSTAGS[0][2]) 
 gmsh.model.addPhysicalGroup(1, [4], PHYSTAGS[0][3]) 
 gmsh.model.addPhysicalGroup(1, [5], PHYSTAGS[0][4]) 
 gmsh.model.addPhysicalGroup(1, [6], PHYSTAGS[0][5]) 
 gmsh.model.addPhysicalGroup(1, [7], PHYSTAGS[0][6]) 
 gmsh.model.addPhysicalGroup(1, [8], PHYSTAGS[0][7])
 gmsh.model.addPhysicalGroup(2, [1], PHYSTAGS[1][0]) 
 gmsh.model.geo.synchronize() 
 gmsh.model.mesh.generate(2)                                 # GENERATE 2D MESH
 #gmsh.fltk.run()                                            # VISUALIZE MESH
 gmsh.write(filename)                                        # SAVE MESH FILE
 gmsh.finalize()                                             # FINALIZE GMSH

def generate_mesh_coax(PATHS,PHYSTAGS,LC,NAME):
 import gmsh
 filename=os.path.join(PATHS[0],NAME+'.msh')
 a       =0.25
 gmsh.initialize()                                           # INITIALIZE GMSH
 gmsh.option.setNumber("General.Terminal"   ,1   )           # MAKE VERBOSE
 gmsh.option.setNumber("Mesh.MshFileVersion",4.11)           # SET MESH OUTPUT VERSION
 #gmsh.option.setNumber("Mesh.SaveAll"       ,1)             # save all mesh elements whether or not physical groups are defined
 gmsh.model.add(NAME)                                        # ADD MODEL
 gmsh.model.geo.addPoint    ( 0, 0, 0, LC, 1)
 gmsh.model.geo.addPoint    (-1, 0, 0, LC, 2)
 gmsh.model.geo.addPoint    ( 1, 0, 0, LC, 3)
 gmsh.model.geo.addPoint    (-a, 0, 0, LC, 4)
 gmsh.model.geo.addPoint    ( a, 0, 0, LC, 5)
 gmsh.model.geo.addCircleArc(2,1,3,1)
 gmsh.model.geo.addCircleArc(3,1,2,2)
 gmsh.model.geo.addCircleArc(4,1,5,3)
 gmsh.model.geo.addCircleArc(5,1,4,4)
 gmsh.model.geo.addCurveLoop([1,2],1)
 gmsh.model.geo.addCurveLoop([3,4],2)
 gmsh.model.geo.addPlaneSurface([1,2], 1)
 gmsh.model.addPhysicalGroup(1, [1,2], PHYSTAGS[0][0]) 
 gmsh.model.addPhysicalGroup(1, [3,4], PHYSTAGS[0][1]) 
 gmsh.model.addPhysicalGroup(2, [1  ], PHYSTAGS[1][0]) 
 gmsh.model.geo.synchronize() 
 gmsh.model.mesh.generate(2)                                 # GENERATE 2D MESH
 #gmsh.fltk.run()                                            # VISUALIZE MESH
 gmsh.write(filename)                                        # SAVE MESH FILE
 gmsh.finalize()                                             # FINALIZE GMSH

def generate_mesh_microstrip(PATHS,PHYSTAGS,LC,NAME):
 import gmsh
 filename=os.path.join(PATHS[0],NAME+'.msh')
 h       =0.375
 w       =0.250
 t       =0.125
 gmsh.initialize()                                           # INITIALIZE GMSH
 gmsh.option.setNumber("General.Terminal"   ,1   )           # MAKE VERBOSE
 gmsh.option.setNumber("Mesh.MshFileVersion",4.11)           # SET MESH OUTPUT VERSION
 #gmsh.option.setNumber("Mesh.SaveAll"       ,1)             # save all mesh elements whether or not physical groups are defined
 gmsh.model.add(NAME)                                        # ADD MODEL 
 gmsh.model.geo.addPoint(0      , 0    , 0, 2*LC, 1 )        # BOUNDING BOX: BOTTOM LEFT
 gmsh.model.geo.addPoint(1      , 0    , 0, 2*LC, 2 )        # BOUNDING BOX: BOTTOM RIGHT
 gmsh.model.geo.addPoint(1      , h-t/2, 0,   LC, 3 )        # DIELECTRIC  : TOP    RIGHT
 gmsh.model.geo.addPoint(1      , 1    , 0, 2*LC, 4 )        # BOUNDING BOX: TOP    RIGHT
 gmsh.model.geo.addPoint(0      , 1    , 0, 2*LC, 5 )        # BOUNDING BOX: TOP    LEFT
 gmsh.model.geo.addPoint(0      , h-t/2, 0,   LC, 6 )        # DIELECTRIC  : TOP    LEFT
 gmsh.model.geo.addPoint(0.5-w/2, h-t/2, 0,   LC, 7 )        # STRIP       : BOTTOM LEFT
 gmsh.model.geo.addPoint(0.5+w/2, h-t/2, 0,   LC, 8 )        # STRIP       : BOTTOM RIGHT
 gmsh.model.geo.addPoint(0.5+w/2, h+t/2, 0,   LC, 9 )        # STRIP       : TOP    RIGHT
 gmsh.model.geo.addPoint(0.5-w/2, h+t/2, 0,   LC, 10)        # STRIP       : TOP    LEFT
 gmsh.model.geo.addLine (1 , 2 , 1 )
 gmsh.model.geo.addLine (2 , 3 , 2 )
 gmsh.model.geo.addLine (3 , 4 , 3 )
 gmsh.model.geo.addLine (4 , 5 , 4 )
 gmsh.model.geo.addLine (5 , 6 , 5 )
 gmsh.model.geo.addLine (6 , 1 , 6 )
 gmsh.model.geo.addLine (7 , 8 , 7 )
 gmsh.model.geo.addLine (8 , 9 , 8 )
 gmsh.model.geo.addLine (9 , 10, 9 )
 gmsh.model.geo.addLine (10, 7 , 10)
 gmsh.model.geo.addLine (3 , 8 , 11)
 gmsh.model.geo.addLine (7 , 6 , 12)
 gmsh.model.geo.addCurveLoop   ([1,2,11,-7 ,12, 6 ],1)
 gmsh.model.geo.addCurveLoop   ([3,4,5 ,-12,7 ,-11],2)
 gmsh.model.geo.addCurveLoop   ([7,8,9 , 10       ],3)
 gmsh.model.geo.addPlaneSurface([1  ],1)
 gmsh.model.geo.addPlaneSurface([2,3],2)
 gmsh.model.addPhysicalGroup(1, [1 ,2 ,3,4 ,5,6], PHYSTAGS[0][0])
 gmsh.model.addPhysicalGroup(1, [7 ,8 ,9,10    ], PHYSTAGS[0][1])
 gmsh.model.addPhysicalGroup(1, [11,12         ], PHYSTAGS[2][0])
 gmsh.model.addPhysicalGroup(2, [2]             , PHYSTAGS[1][0])
 gmsh.model.addPhysicalGroup(2, [1]             , PHYSTAGS[1][1])
 gmsh.model.geo.synchronize() 
 gmsh.model.mesh.generate(2)                                 # GENERATE 2D MESH
 #gmsh.fltk.run()                                            # VISUALIZE MESH
 gmsh.write(filename)                                        # SAVE MESH FILE
 gmsh.finalize()                                             # FINALIZE GMSH

def parse_gmsh_mesh_file(PATHS,PARAMS):
 
 from utilsd import (rFILE           ,
                     get_num_from_str,
                     export_mesh      )
 
 def get_mesh_file_segments(MSH):
  index_entities=[]
  index_nodes   =[]
  index_elements=[]
  for i in range(len(MSH)):
   if MSH[i]=='$Entities':
    index_entities.append(i+1)
   else:
    if MSH[i]=='$EndEntities':
     index_entities.append(i)
    else:
     if MSH[i]=='$Nodes':
      index_nodes.append(i+1)
     else:
      if MSH[i]=='$EndNodes':
       index_nodes.append(i)
      else:
       if MSH[i]=='$Elements':
        index_elements.append(i+1)
       else:
        if MSH[i]=='$EndElements':
         index_elements.append(i)
  Entities=MSH[index_entities[0]:index_entities[1]]
  Nodes   =MSH[index_nodes   [0]:index_nodes   [1]]
  Elements=MSH[index_elements[0]:index_elements[1]] 
  return Entities,Nodes,Elements
 
 def map_0d_element_tags_to_1d_physical_group_tags(ENTITIES):
  tmp            =get_num_from_str([ENTITIES[0]], True)[0]
  num_0d_elements=tmp[0]
  num_1d_elements=tmp[1]
  index_start    =tmp[0]+1
  index_end      =index_start+tmp[1]
  tmp            =get_num_from_str( ENTITIES[index_start:index_end])
  tmp            =[[int(e[8]),[abs(int(e[10])),abs(int(e[11]))]] for e in tmp]
  mappings       =[]
  for  i in range(num_0d_elements):
   TMP=[]
   for j in range(num_1d_elements):
    if i+1 in tmp[j][1]:
     TMP.append(tmp[j][0])
   mappings.append(TMP)
  return mappings
 
 def map_1d_element_tags_to_1d_physical_group_tags(ENTITIES): 
  tmp        =get_num_from_str([ENTITIES[0]], True)[0]
  index_start=tmp[0]+1
  index_end  =index_start+tmp[1]
  tmp        =get_num_from_str( ENTITIES[index_start:index_end])
  return [int(e[8]) for e in tmp]
 
 def map_2d_element_tags_to_2d_physical_group_tags(ENTITIES):
  tmp        =get_num_from_str([ENTITIES[0]], True)[0]
  index_start=tmp[0]+tmp[1]+1
  index_end  =index_start+tmp[2]
  tmp        =get_num_from_str( ENTITIES[index_start:index_end])
  return [int(e[8]) for e in tmp]
 
 def get_mesh_file_nodes(NODES,MAPPINGS):
  segments            =get_num_from_str([NODES[0]], True)[0][0]
  index_segment_begin =1
  nodes               =[]
  
  for _ in range(segments):
   segment_info       =get_num_from_str([NODES[index_segment_begin]], True)[0]
   
   mapping            =MAPPINGS[segment_info[0]]
   
   index_tags_end     =index_segment_begin+  segment_info[3]+1
   index_segment_end  =index_segment_begin+2*segment_info[3]+1  
   
   nodes_tmp          =get_num_from_str(NODES[index_tags_end  :index_segment_end]      )
   for i in range(len(nodes_tmp)):
    nodes_tmp[i].append(mapping[segment_info[1]-1])
   
   nodes              =nodes+nodes_tmp
   index_segment_begin=index_segment_end
  return nodes
 
 def get_mesh_file_elements(ELEMENTS,MAPPING):
  segments            =get_num_from_str([ELEMENTS[0]], True)[0][0]
  index_segment_begin =1
  elements            =[]
  
  for _ in range(segments):
   segment_info       =get_num_from_str([ELEMENTS[index_segment_begin]], True)[0]
   index_segment_end  =index_segment_begin+segment_info[3]+1
   if segment_info[0]==2:
    elements_tmp      =get_num_from_str(ELEMENTS[index_segment_begin+1:index_segment_end], True)
    elements_tmp      =[e[1:]                          for e in elements_tmp]
    elements_tmp      =[ [i-1 for i in e]              for e in elements_tmp]
    elements_tmp      =[e+[MAPPING[segment_info[1]-1]] for e in elements_tmp]
    elements          =elements+elements_tmp
   
   index_segment_begin=index_segment_end
  return elements
 
 def remove_partition_tag_from_nodes_and_tags(NODES,MAP0D,MAP1D,PARAMS):  
  map_0d_filtered=[[nn for nn in n if not nn in PARAMS[5]] for n in MAP0D                      ]
  map_1d_filtered=[ n                                      for n in MAP1D if not n in PARAMS[5]]
  nodes          =[ n                                      for n in NODES                      ]
  for i in range(len(nodes)):
   if isinstance (nodes[i][3],list):
    try:
     nodes[i][3].remove(PARAMS[5][0])
    except ValueError:
     pass
  return map_0d_filtered, map_1d_filtered, nodes
 
 mesh_file                =rFILE(os.path.join(PATHS[0],PARAMS[2][1]+'.msh'))
 Entities, Nodes, Elements=get_mesh_file_segments(mesh_file)
 map_0d                   =map_0d_element_tags_to_1d_physical_group_tags(Entities)
 map_1d                   =map_1d_element_tags_to_1d_physical_group_tags(Entities)
 map_2d                   =map_2d_element_tags_to_2d_physical_group_tags(Entities)
 nodes                    =get_mesh_file_nodes                          (Nodes   ,[map_0d,map_1d,map_2d])
 elements                 =get_mesh_file_elements                       (Elements, map_2d               )
 if PARAMS[5]:
  map_0d, map_1d, nodes   =remove_partition_tag_from_nodes_and_tags     (nodes   , map_0d,map_1d,PARAMS )
 export_mesh(nodes,elements,PATHS)
 return nodes, elements, [*map_0d,*map_1d]


