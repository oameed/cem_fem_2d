#########################################
### FINITE ELEMENT METHOD (FEM)       ###
### DIRICHLET BOUNDARY VALUE PROBLEMS ###
### PARAMETER DEFINITIONS             ###
### by: OAMEED NOAKOASTEEN            ###
#########################################

import os
import argparse
import numpy    as np

### DEFINE PARSER
parser=argparse.ArgumentParser() 

### DEFINE PARAMETERS
parser.add_argument('-p'   , type=int  ,             help='PROBLEM'                                                 , required=True )
parser.add_argument('-v'   , type=str  ,             help='EXPERIMENT VERSION'                                      , required=True )
parser.add_argument('-ns'  , type=int  , default=10, help='NUMBER OF SOLUTIONS TO POSTPROCESS IN EIGENMODE PROBLEMS'                )

### ENABLE FLAGS
args  = parser.parse_args()

### CONSTRUCT PARAMETER STRUCTURES

eps0  = 8.85e-12
mu0   = 4*np.pi*1e-7

PATHS = [os.path.join('..','..','experiments',args.v)]

PARAMS=[[args.p, args.v, args.ns],
        [eps0  , mu0            ] ]


# PARAMS[0][0]: PROBLEM
# PARAMS[0][1]: EXPERIMENT VERSION
# PARAMS[0][2]: NUMBER OF SOLUTIONS TO POSTPROCESS IN EIGENMODE PROBLEMS

# PARAMS[1][0]: FREE SPACE PERMITTIVITY
# PARAMS[1][1]: FREE SPACE PERMEABILITY
# PARAMS[1][2]: FREQUENCY                                                    !!! ADDED BY THE 'set_up_the_problem' FUNCTION
# PARAMS[1][3]: FREE SPACE WAVE NUMBER                                       !!! ADDED BY THE 'set_up_the_problem' FUNCTION

# PARAMS[2][0]: PROBLEM TYPE SELECTOR FOR ALPHA & BETA (2D TEz          :1, 
#                                                       2D TMz          :2, 
#                                                       2D ELECTROSTATIC:3, 
#                                                       2D MAGNETOSTATIC:4 ) !!! ADDED BY THE 'set_up_the_problem' FUNCTION

# PARAMS[3][*]: PHYSICAL GROUP TAGS                                          !!! ADDED BY THE 'set_up_the_problem' FUNCTION
# PARAMS[4][*]: PHYSICAL GROUP TAG  VLAUES                                   !!! ADDED BY THE 'set_up_the_problem' FUNCTION
# PARAMS[5][0]: 2D PARTIION TAG                                              !!! ADDED BY THE 'set_up_the_problem' FUNCTION


