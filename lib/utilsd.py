#########################################
### FINITE ELEMENT METHOD (FEM)       ###
### DIRICHLET BOUNDARY VALUE PROBLEMS ###
### UTILITY FUNCTION DEFINITIONS      ###
### by: OAMEED NOAKOASTEEN            ###
#########################################

import os
import numpy as np

def rFILE(FILENAME):
 with open(FILENAME) as fobj:  
  lines=[l.strip() for l in fobj.readlines()] 
 return lines

def wHDF(FILENAME,DIRNAMES,DATA):
 import h5py
 fobj=h5py.File(FILENAME,'w')
 for i in range(len(DIRNAMES)):
  fobj.create_dataset(DIRNAMES[i],data=DATA[i])
 fobj.close()

def get_num_from_str(STR, INT=False):
 temp=[]
 if not INT:
  for i in range(len(STR)):
   temp.append([float(i) for i in STR[i].split(' ')])
 else:
  for i in range(len(STR)):
   temp.append([int  (i) for i in STR[i].split(' ')]) 
 return temp

def export_mesh(NODES,ELEMENTS,PATHS):
 filename=os.path.join(PATHS[0],'mesh'+'.h5')
 nodes   =[node                  [:3] for node    in NODES    ]
 elements=[[e+1 for e in element][:3] for element in ELEMENTS ]
 nodes   =np.array(nodes   )
 elements=np.array(elements)
 
 wHDF(filename,
      ['nodes','elements'],
      [ nodes , elements ] )

def export_global_matrix_unknowns(X,PATHS):
 filename=os.path.join(PATHS[0],'global_matrix_unknowns'+'.h5')
 wHDF(filename,
      ['x'],
      [ X ] )


