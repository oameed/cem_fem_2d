#########################################
### FINITE ELEMENT METHOD (FEM)       ###
### DIRICHLET BOUNDARY VALUE PROBLEMS ###
### 1D SOLVER                         ###
### by: OAMEED NOAKOASTEEN            ###
#########################################

import os
import sys
import argparse
import numpy                  as np
from matplotlib import pyplot as plt

parser   =argparse.ArgumentParser()
parser.add_argument('-v', type=str, required=True )
args     =parser.parse_args()
PATH     =os.path.join('..','..','experiments',args.v)

L        =[0,1]                                                                # THE INTERVAL DEFINING THE COMPUTATIONAL DOMAIN
Ub       = 0                                                                   # DIRICHLET B.C. AT THE BEGINNING OF THE INTERVAL
Ue       = 0                                                                   # DIRICHLET B.C. AT THE END       OF THE INTERVAL
f_func   =lambda x: np.power(x,2)                                              # FORCING FUNCTION
UAn      =lambda x: (1/np.sin(1))*(np.sin(x)+2*np.sin(1-x))+np.power(x,2)-2    # THE ANALYTIC SOLUTION

alpha    =1
beta     =1

node_size=100                                                                  # NUMBER OF NODES
nodes    =np.linspace(L[0],L[1],node_size)
mesh     =[[i,i+1] for i in range(node_size-1)]
matrix   =np.zeros((node_size,node_size))
vector   =np.zeros((node_size          ))

for i in range(len(mesh)):
 dx      = nodes[mesh[i][1]]-nodes[mesh[i][0]]
 midpoint=(nodes[mesh[i][1]]+nodes[mesh[i][0]])/2
 M       =(1/dx)   *alpha           *np.array([[-1 , 1 ],
                                               [ 1 ,-1 ] ])                    # GET ELEMENT MODEL
 T       =(1/6 )*dx*beta            *np.array([[ 2 , 1 ],
                                               [ 1 , 2 ] ])
 K       =M+T
 f       =(1/2 )*dx*f_func(midpoint)*np.array([  1 , 1   ])
 matrix[mesh[i][0],mesh[i][0]]+=K[0,0]                                         # CONSTRUCT GLOBAL MATRIX
 matrix[mesh[i][0],mesh[i][1]]+=K[0,1]
 matrix[mesh[i][1],mesh[i][0]]+=K[1,0]
 matrix[mesh[i][1],mesh[i][1]]+=K[1,1]
 vector[mesh[i][0]           ]+=f[0  ]                                         # CONSTRUCT GLOBAL VECTOR
 vector[mesh[i][1]           ]+=f[1  ]

vector  -=matrix[:, 0]*Ub                                                      # IMPOSE BOUNDARY CONDITIONS
vector  -=matrix[:,-1]*Ue
matrix   =np.delete(matrix,[0,node_size-1],axis=0)                        
matrix   =np.delete(matrix,[0,node_size-1],axis=1)
vector   =np.delete(vector,[0,node_size-1]       ) 

U        =np.linalg.solve(matrix,vector)                                       # SOLVE

plt.plot   (nodes[1:-1],U                                 )
plt.plot   (nodes      ,UAn(nodes), '--'                  )
plt.legend (('Numerical','Analytic')                      )
plt.savefig(os.path.join(PATH,'data'+'.png'), format='png')


