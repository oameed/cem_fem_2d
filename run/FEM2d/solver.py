#########################################
### FINITE ELEMENT METHOD (FEM)       ###
### DIRICHLET BOUNDARY VALUE PROBLEMS ###
### SOLVER                            ###
### by: OAMEED NOAKOASTEEN            ###
#########################################

import os
import sys
import numpy as np
import scipy as sp
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
from paramd import  PATHS, PARAMS
from meshd  import  parse_gmsh_mesh_file
from maind  import (set_up_the_problem                  ,
                    global_matrix_assembler             ,
                    impose_dirichlet_boundary_conditions,
                    get_solution_complete               ,
                    post_processing                      )

########################################################################
### DEFINE THE PROBLEM                                               ###
### (1) define physical groups; assign tags and values.              ###
###     NOTE: 1D physical groups represent dirichlet b.c. along      ###
###           1D boundaries.                                         ###
###     NOTE: 2D physical groups represent 2D regions (surfaces)     ###
###           with uniform constitutive parameters.                  ### 
###     NOTE: 2D partition tag is used to define 1D lines that       ###
###           partition the computational domain into multiple       ###
###           regions.                                               ###
### (2) generate mesh                                                ###
########################################################################
set_up_the_problem(PATHS, PARAMS)

########################################################################
### PARSE THE MESH FILE:                                             ###
### (1) get the list of nodes:                                       ###
###     each element in the node list contains the coordinates       ###
###     of a node plus the 1d/2d tag associated with that node.      ###
### (2) get the list of elements:                                    ###
###     each element in the element list contains the global numbers ###
###     of the three nodes of a triangle and the 2d tag associated   ###
###     with that triangle.                                          ###
########################################################################
(nodes                                ,
 elements                             ,
 tags_for_nodes_on_dirichlet_boudaries )=parse_gmsh_mesh_file                (PATHS , 
                                                                              PARAMS )

########################################################################
### ASSEMBLE THE GLOBAL MATRIX OF UNKNOWNS:                          ###
### (1) for each element get the alpha, beta and element model.      ###
### (2) add components of the element model to the global matrix.    ###
########################################################################
global_matrix_unknowns                  =global_matrix_assembler             (PATHS   ,
                                                                              PARAMS  , 
                                                                              nodes   , 
                                                                              elements )

########################################################################
### IMPOSE DIRICHLET BOUNDARY CONDITION:                             ###
### (1) for each node on the dirichlet boundary, calculate the       ###
###     corresponding contribution to the  source vector and         ###
###     add it to the source vector.                                 ###
### (2) for each node on the dirichlet boundary, remove the          ###
###     corresponding row and colomn from the global matrix and      ###
###     remove the corresponding row from the source vector.         ###
########################################################################
(global_matrix_unknowns,
 source_vector         ,
 index_splits           )               =impose_dirichlet_boundary_conditions(PARAMS                               ,
                                                                              nodes                                ,
                                                                              tags_for_nodes_on_dirichlet_boudaries,
                                                                              global_matrix_unknowns                )

########################################################################
### SOLVE THE LINEAR SYSTEM & POST-PROCESS THE SOLUTIONS             ###
########################################################################
if np.all(source_vector==0):
 (eigen_values , 
  eigen_vectors )=np.linalg.eig  (global_matrix_unknowns)
 sorting_indeces =np.argsort(abs(eigen_values)).tolist()
 eigen_values    =eigen_values [  sorting_indeces]
 eigen_vectors   =eigen_vectors[:,sorting_indeces]
 solution        =eigen_vectors[:,:PARAMS[0][2]  ] 
else:
 solution        =np.linalg.solve(global_matrix_unknowns,source_vector)
 solution        =np.reshape(solution,[-1,1])
 
solution         =get_solution_complete(PARAMS      ,
                                        nodes       ,
                                        index_splits,
                                        solution     )

post_processing(PATHS   ,
                PARAMS  ,
                nodes   ,
                elements,
                solution )

print("FNISHED")


