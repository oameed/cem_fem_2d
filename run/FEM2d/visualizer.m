%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FINITE ELEMENT METHOD (FEM)                  %%%
%%% DIRICHLET BOUNDARY VALUE PROBLEMS            %%%
%%% MESH & GLOBAL MATRIX VILSUALIZATION FUNCTION %%%
%%% by: OAMEED NOAKOASTEEN                       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function visualizer(TYPE,VER,RATIO)
close all
clc

if strcmp(TYPE,'mesh')
    filename    =fullfile('..','..','experiments',VER,'mesh')                          ;
    nodes       =permute(h5read(strcat(filename,'.h5'),fullfile('/','nodes'   )),[2,1]);
    elements    =permute(h5read(strcat(filename,'.h5'),fullfile('/','elements')),[2,1]);
    fig         =figure                                                                ;
    fig.Renderer='painters'                                                            ;
    position    =fig.Position                                                          ;
    set(fig,'Position',[position(1),position(2),position(3),RATIO*position(3)])
    patch('Faces'    ,elements,...
          'Vertices' ,nodes   ,...
          'EdgeColor','k'     ,...
          'FaceColor','none'      )
    set(gca,'xticklabel',[]   ,...
            'yticklabel',[]   ,...
            'zticklabel',[]   ,...
            'xtick'     ,[]   ,...
            'ytick'     ,[]   ,...
            'ztick'     ,[]   ,...
            'Visible'   ,'off'    )
    saveas(gca,strcat(filename,'.svg'),'svg')
    close all
else
    if strcmp(TYPE,'matrix')
        filename    =fullfile('..','..','experiments',VER,'global_matrix_unknowns')    ;
        matrix      =permute(h5read(strcat(filename,'.h5'),fullfile('/','x')),[2,1])   ;
        fig         =figure                                                            ;
        fig.Renderer='painters'                                                        ;
        position    =fig.Position                                                      ;
        set(fig,'Position',[position(1),position(2),position(3),RATIO*position(3)])
        spy(matrix)
        saveas(gca,strcat(filename,'.svg'),'svg')
        close all
    end
end

end

