
conda activate pywin

cd run\FEM1d

echo "CREATE PROJECT DIRECTORY"

$FileName=                               "..\..\experiments\v31"
if (Test-Path $FileName) {
  rm -Recurse -Force $FileName
}
tar -xvzf ..\..\experiments\v00.tar.gz -C ..\..\experiments
mv        ..\..\experiments\v00           ..\..\experiments\v31

echo "SOLVE 1D FEM PROBLEM"
python solver.py -v v31

cd ..\..\


