#! /bin/bash

source activate gmshpy

cd run/FEM2d

rm     -rf                                   ../../experiments/v11
tar    -xzf  ../../experiments/v00.tar.gz -C ../../experiments
mv           ../../experiments/v00           ../../experiments/v11

echo ' SOLVEING EIGENMODES FOR A RECTANGULAR CAVITY '
python solver.py -p 1 -v v11 

matlab -nodisplay -nosplash -nodesktop -r "visualizer('mesh'  ,'v11',0.5);exit;"
matlab -nodisplay -nosplash -nodesktop -r "visualizer('matrix','v11',1  );exit;"

cd                                           ../../experiments/v11
NUM=10
for ((c=1; c<=$NUM; c++))
do
 echo -e "Include 'data_$c"_E".pos';\nPrint 'data_$c"_E".png';\nExit;" > ExportFigure.geo; gmsh ExportFigure.geo
 echo -e "Include 'data_$c"_H".pos';\nPrint 'data_$c"_H".png';\nExit;" > ExportFigure.geo; gmsh ExportFigure.geo
done



