#! /bin/bash

source activate gmshpy

cd run/FEM2d

rm     -rf                                   ../../experiments/v12
tar    -xzf  ../../experiments/v00.tar.gz -C ../../experiments
mv           ../../experiments/v00           ../../experiments/v12

echo ' SOLVEING EIGENMODES FOR A CIRCULAR CAVITY '
python solver.py -p 2 -v v12

matlab -nodisplay -nosplash -nodesktop -r "visualizer('mesh'  ,'v12',1);exit;"
matlab -nodisplay -nosplash -nodesktop -r "visualizer('matrix','v12',1);exit;"

cd                                           ../../experiments/v12
NUM=10
for ((c=1; c<=$NUM; c++))
do
 echo -e "Include 'data_$c"_E".pos';\nPrint 'data_$c"_E".png';\nExit;" > ExportFigure.geo; gmsh ExportFigure.geo
 echo -e "Include 'data_$c"_H".pos';\nPrint 'data_$c"_H".png';\nExit;" > ExportFigure.geo; gmsh ExportFigure.geo
done



