#! /bin/bash

source activate gmshpy

cd run/FEM2d

rm     -rf                                   ../../experiments/v23
tar    -xzf  ../../experiments/v00.tar.gz -C ../../experiments
mv           ../../experiments/v00           ../../experiments/v23

echo ' SOLVEING ELECTROSTATIC POTENTIAL FOR THE MICROSTRIP'
python solver.py -p 5 -v v23

matlab -nodisplay -nosplash -nodesktop -r "visualizer('mesh'  ,'v23',1);exit;"
matlab -nodisplay -nosplash -nodesktop -r "visualizer('matrix','v23',1);exit;"

cd                                           ../../experiments/v23
echo -e "Include 'data_1_V.pos';\nPrint 'data_1_V.png';\nExit;" > ExportFigure.geo; gmsh ExportFigure.geo
echo -e "Include 'data_1_E.pos';\nPrint 'data_1_E.png';\nExit;" > ExportFigure.geo; gmsh ExportFigure.geo


