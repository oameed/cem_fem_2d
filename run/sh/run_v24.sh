#! /bin/bash

source activate gmshpy

cd run/FEM2d

rm     -rf                                   ../../experiments/v24
tar    -xzf  ../../experiments/v00.tar.gz -C ../../experiments
mv           ../../experiments/v00           ../../experiments/v24

echo ' SOLVEING ELECTROSTATIC POTENTIAL FOR THE 2D POTENTIAL BOX'
python solver.py -p 6 -v v24

matlab -nodisplay -nosplash -nodesktop -r "visualizer('mesh'  ,'v24',0.5);exit;"
matlab -nodisplay -nosplash -nodesktop -r "visualizer('matrix','v24',1  );exit;"

cd                                           ../../experiments/v24
echo -e "Include 'data_1_V.pos';\nPrint 'data_1_V.png';\nExit;" > ExportFigure.geo; gmsh ExportFigure.geo
echo -e "Include 'data_1_E.pos';\nPrint 'data_1_E.png';\nExit;" > ExportFigure.geo; gmsh ExportFigure.geo


