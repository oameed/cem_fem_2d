#! /bin/bash

source activate gmshpy

cd run/FEM1d

rm     -rf                                   ../../experiments/v31
tar    -xzf  ../../experiments/v00.tar.gz -C ../../experiments
mv           ../../experiments/v00           ../../experiments/v31

python solver.py -v v31


